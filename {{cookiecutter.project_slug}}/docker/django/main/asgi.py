"""
ASGI config for the {{ cookiecutter.project_slug }} project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

# isort: off
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main.settings")
# isort: on

from django.core.asgi import get_asgi_application

from channels.routing import ProtocolTypeRouter
from channels.routing import ChannelNameRouter, ProtocolTypeRouter, URLRouter
from channels.sessions import SessionMiddlewareStack

from channels.security.websocket import AllowedHostsOriginValidator

from dasf_broker.token_auth_middleware import TokenAuthMiddlewareStack

from main.routing import websocket_urlpatterns
from main.workers import workers

application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": AllowedHostsOriginValidator(
            SessionMiddlewareStack(
                TokenAuthMiddlewareStack(
                    URLRouter(websocket_urlpatterns)
                )
            )
        ),
        "channel": ChannelNameRouter(workers),
    }
)
